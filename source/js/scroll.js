var goScroll = function(options) {
	var scrollBar = options.scrollTarget || 'body, html';
	var scrollTopVal = $(options.targetID).offset().top - 130;
	window.setTimeout(function() {
		$(scrollBar).animate({
			scrollTop: scrollTopVal
		}, 300);
	});
};


$(function() {
	$('body').css('opacity', 0).animate({
		opacity: 1
	}, 750);

	$('.story').css('height', $(window).height() - 150);

	$('#js-pageLink').on('click', '.js-scroll', function(e) {
		$('#js-pageLink').find('.js-scroll').removeClass('current');
		var moveUrl = $(this).attr('href');
		e.preventDefault();
		goScroll({
			'targetID': moveUrl,
			'scrollTarget': 'body'
		});
		$(this).addClass('current');
	});
});
